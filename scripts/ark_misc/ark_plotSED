#!/usr/bin/python3

import numpy as np
import argparse
from matplotlib import pylab as plt
import h5py


####################################################################
# option parser
####################################################################

description="""Plot SEDs from hdf5 files
"""

epilog     ="""
Examples:
--------
ark_plotSED SED.hdf5
ark_plotSED --normalize SED1.hdf5 SED2.hdf5
"""

parser = argparse.ArgumentParser(description=description,epilog=epilog,formatter_class=argparse.RawDescriptionHelpFormatter)


parser.add_argument(action="store", 
                    dest="files", 
                    metavar='FILENAMES', 
                    type=str,
                    default=None,
                    nargs='*',
                    help='the list of snapshots') 

parser.add_argument("-o",
                    action="store",
                    type=str,
                    dest="outputfilename",
                    default=None,
                    help="Name of the output file")  

                    
parser.add_argument("--normalize",
                    action="store_true", 
                    default=False,
                    help='normalize')                     
                                
parser.add_argument('--normalization_wavelength',
                    action="store", 
                    dest="normalization_wavelength", 
                    metavar='FLOAT', 
                    type=float,
                    default=9000,
                    help='normalization wavelength')
                    
parser.add_argument('--xmin',
                    action="store", 
                    dest="xmin", 
                    metavar='FLOAT', 
                    type=float,
                    default=2000,
                    help='x min')

parser.add_argument('--xmax',
                    action="store", 
                    dest="xmax", 
                    metavar='FLOAT', 
                    type=float,
                    default=16000,
                    help='x max')
                    
parser.add_argument('--ymin',
                    action="store", 
                    dest="ymin", 
                    metavar='FLOAT', 
                    type=float,
                    default=None,
                    help='y min')

parser.add_argument('--ymax',
                    action="store", 
                    dest="ymax", 
                    metavar='FLOAT', 
                    type=float,
                    default=None,
                    help='y max')

parser.add_argument('--log',
                    action="store", 
                    dest="log", 
                    metavar='STR', 
                    type=str,
                    default=None,
                    help='log scale (None,x,y,xy)')


def SetAxis(ax,xmin, xmax, ymin, ymax, log=None):
  """
  Set ticks for the axis
  """
  
  # slightly extend the limits
  if xmin is not None and xmax is not None:
    if xmin == xmax:
        xmin = xmin - 0.05 * xmin
        xmax = xmax + 0.05 * xmax
    else:
        xmin = xmin - 0.05 * (xmax - xmin)
        xmax = xmax + 0.05 * (xmax - xmin)  
  
  if ymin is None and ymax is None:
    ymin,ymax = ax.get_ylim()
    
  if ymin == ymax:
      ymin = ymin - 0.05 * ymin
      ymax = ymax + 0.05 * ymax
  else:
      ymin = ymin - 0.05 * (ymax - ymin)
      ymax = ymax + 0.05 * (ymax - ymin)
  
       


  plt.axis([xmin, xmax, ymin, ymax])


  if log is None:
      log = 'None'

  if str.find(log, 'x') == -1:
      ax.xaxis.set_major_locator(plt.AutoLocator())
      x_major = ax.xaxis.get_majorticklocs()
      dx_minor = (x_major[-1] - x_major[0]) / (len(x_major) - 1) / 5.
      ax.xaxis.set_minor_locator(plt.MultipleLocator(dx_minor))


  if str.find(log, 'y') == -1:
      ax.yaxis.set_major_locator(plt.AutoLocator())
      y_major = ax.yaxis.get_majorticklocs()
      dy_minor = (y_major[-1] - y_major[0]) / (len(y_major) - 1) / 5.
      ax.yaxis.set_minor_locator(plt.MultipleLocator(dy_minor))



                    
####################################################################
# main
####################################################################



def Do(opt):
  

    


  params = {
    "axes.labelsize": 14,
    "axes.titlesize": 18,
    "font.size": 12,
    "legend.fontsize": 12,
    "xtick.labelsize": 14,
    "ytick.labelsize": 14,
    "text.usetex": True,
    "figure.subplot.left": 0.15,
    "figure.subplot.right": 0.95,
    "figure.subplot.bottom": 0.15,
    "figure.subplot.top": 0.95,
    "figure.subplot.wspace": 0.02,
    "figure.subplot.hspace": 0.02,
    "figure.figsize" : (8, 6),
    "lines.markersize": 6,
    "lines.linewidth": 2.0,
  }
  plt.rcParams.update(params)



  # create the plot
  fig = plt.gcf()
  fig.set_size_inches(8,6)
  ax  = plt.gca()







        
  #############################################
  # Open the SED files
  ############################################# 
  
  for File in opt.files: 
    

    with h5py.File(File, 'r') as f:
    
      SED = f["Data/SED"][:]
      lmb = f["Data/wavelength"][:]
      
      
      if opt.normalize:
        idx = np.argmin(np.fabs(lmb-opt.normalization_wavelength))
        SED = SED/SED[idx]
        
 
      ax.plot(lmb,SED)
  
  


  # set the axis
  SetAxis(ax,opt.xmin,opt.xmax,opt.ymin,opt.ymax,log=opt.log)
  
  ax.set_xlabel("wavelength [A]")
  ax.set_ylabel("SED [erg/s/Angstrom]")
  
  # save or display
  if opt.outputfilename:
    plt.savefig(opt.outputfilename)
  else:
    plt.show()  
  
  


if __name__ == '__main__':
  
  opt = parser.parse_args()
  
  Do(opt)
 
