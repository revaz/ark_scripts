#!/usr/bin/env python3
###########################################################################################
#  package:   ark_scripts
#  file:      setup.py
#  brief:     Install ark_scripts
#  copyright: GPLv3
#             Copyright (C) 2023 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of ark_scripts.
###########################################################################################

from setuptools import setup, find_packages, Extension
import glob
import os


# scripts
scripts = []
for o in glob.glob('./scripts/**', recursive=True):
  if os.path.isfile(o):
    scripts.append(o)

# data
package_files = []
for o in glob.glob('./data/**', recursive=True):
  if os.path.isfile(o):
    package_files.append(o)


setup(
    name="ark_scripts",
    author="Yves Revaz",
    author_email="yves.revaz@epfl.ch",
    url="https://gitlab.com/revaz/ark_scripts",
    description="""arrakihs module""",
    license="GPLv3",
    version="0.1",

    packages=find_packages(),
    scripts=scripts,
    package_data={'ark_scripts': package_files},
    install_requires=["numpy", "scipy", "h5py",
                      "astropy", "pNbody", "matplotlib",
                      "tqdm"],

)
